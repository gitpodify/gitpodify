module.exports = {
    extends: ["@commitlint/config-conventional"],
    rules: {
        "signed-off-by": [2, "always"], // always require sign-offs
        "body-max-line-length": [1, "always", 100], // rebasing stuff maybe chaos, so we'll downgrade this rule to warning from error
    },
};
