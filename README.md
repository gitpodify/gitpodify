# Gitpodify

A CLI to help manage your Gitpod configuration easily, even without opening a file editor for most cases or firing a new Gitpod workspace.

:warning: **Project under depvelopment:** Things might break as we develop the CLI for features and stability. Proceed at your own risk. Everything you see here is work in progress, as always.

## Features

The feature set is currently work in progress, but here are some:

* Easily generate an new `.gitpod.yml` configuration file, just like `gp init --interactive` inside an Gitpod workspace.
* Add and remove VS Code extensions on both `.vscode/extensions.json` under `recommendations` key and on `.gitpod.yml` under `vscode.extensions`.
* Build your custom Gitpod workspace image from your Dockerfile defined in `image.file` with BuildKit enabled by default, no more adding `DOCKER_BUILDKIT=1 docker build ...` to your `package.json`'s scripts.

## Install

Since Gitpodify CLI is an TypeScript-based package, you need to install it globally through `npm i -g gitpodify`.

You can also install the CLI on per-project basis, good for users of Yarn Modern:

```bash
# install to your project with Yarn Modern
yarn add gitpodify --dev

# ...and call the CLI entrypoint with yarn prefixed.
yarn gitpodify help
```

You can also add the CLI into your `.gitpod.yml` (or even `.gitpod.Dockerfile`) if you want, especially if you manage multiple repos in one Gitpod workspace.

```yaml
# in your Gitpod configuration
- init: |
    # other scripts go here #

    npm i -g gitpodify
```

```Dockerfile
# or in your .gitpod.Dockerfile
FROM gitpod/workspace-full

# your other parts of Dockerfile here...

RUN npm i -g gitpodify
```

## Support Cycle

Since the project is under development, we only support the latest minor version in the `0.x` series for now due to limited manpower.

We only support the last two LTS versions of Node.js, plus even-numbered Current (aka future LTS version) release for now, but we assume this work on atleast Node.js 10.x.

## License

[MIT](LICENSE)

## Disclaimer

This project is NOT associated with Gitpod GmbH.
