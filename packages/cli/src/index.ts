import { createCommand } from 'commander';
import { yaml } from 'mrm-core';
import { version } from "../package.json";
const program = createCommand();

const cli = program.description("A CLI to help manage your Gitpod configuration, without requiring you to create yet another Gitpod workspace or even opening an text editor.")
    .version(version)
    .name("gitpodify")
    .showHelpAfterError('(add --help for additional information and to show available commands)')
    .showSuggestionAfterError();

// Define the Gitpod config filename globally, so we can call it globally.
const gitpodConfig = yaml('.gitpod.yml')

function initConfig(dockerImage: string) {
    if (!gitpodConfig.exists() && dockerImage == undefined) {
        gitpodConfig.merge({
            image: "gitpod/workspace-full",
            tasks: [
                {
                    init: "echo todo",
                    command: "echo todo"
                }
            ]
        })
        .save();
    } else if (!gitpodConfig.exists() && dockerImage !== undefined) {
        gitpodConfig.merge({
            image: dockerImage,
            tasks: [
                {
                    init: "echo todo",
                    command: "echo todo"
                }
            ]
        })
        .save();
    }
}

cli.command('init [image]')
    .description("Initialize an bare-bones Gitpod configuration, optionally set an optional Docker image for Gitpod workspaces.")
    .action((initImage) => {
        initConfig(initImage)
    })

program.parse();
