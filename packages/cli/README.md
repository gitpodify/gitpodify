# Gitpodify CLI

## Install

```bash
npm i -g @gitpodify/cli # note that you can run "npm i -g gitpodify" instead.
```

## Build from source

You don't need to do `yarn install` since we implemented Zero-Installs in the monorepo. You may opt to run this command if needed, especially to generate build artifacts for specific OSes.

1. Run the `cli:prettier-verify` and `cli:lint` Yarn scripts to check there are no code style and linter errors.
2. If confimed that everything is fine, run the `cli:build` Yarn script to call the TypeScript complier.
3. Finally, you can now call your built-from-source Gitpodify CLI through the `cli:entrypoint` Yarn script. Note that since we're using Plug 'n Play in the monorepo, you may need to set `NODE_OPTIONS` variable into `--require /path/to/gitpodify-monorepo/.pnp.cjs` manually.
